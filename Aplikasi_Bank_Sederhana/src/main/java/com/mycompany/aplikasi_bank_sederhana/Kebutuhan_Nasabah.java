package Aplikasi_Bank_Sederhana;
public class Keperluan_Nasabah {
    private int NoRek;
    private String NamaCalonNasabah;
    private String AlamatNasabah;
    int TabunganAwal;
    private double UangMenabung;
    private double UangTransfer;
    private double NoRekTujuan; 
    private double NewNoRek;
    private double NoTelepon;
    private int UangPenarik;
    
    Keperluan_Nasabah(int tabunganAwal){
        this.TabunganAwal=tabunganAwal;
    }

    Keperluan_Nasabah(int NoRek, double UangMenabung) {
        this.NoRek = NoRek;
        this.UangMenabung = UangMenabung;
    }

    Keperluan_Nasabah(int NoRek, int UangPenarik) {
        this.NoRek = NoRek;
        this.UangPenarik = UangPenarik;
    }

    Keperluan_Nasabah(int NoRek, double UangTransfer, double NoRekTujuan) {
        this.NoRek = NoRek;
        this.UangTransfer = UangTransfer;
        this.NoRekTujuan = NoRekTujuan;
    }

    Keperluan_Nasabah(double NewNoRek, double NoTelepon, String NamaCalonNasabah, String AlamatNasabah) {
        this.NewNoRek = NewNoRek;
        this.NoTelepon = NoTelepon;
        this.NamaCalonNasabah = NamaCalonNasabah;
        this.AlamatNasabah = AlamatNasabah;

    }

    
}
